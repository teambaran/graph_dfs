/****************************************************************************************
/**
*	FindComponents.c : Contains main for pa5.
*	Name: Kaile Baran
*	Student ID: kbaran
*	Assignment: Programming Assignment 5
*	Course: CMPS 101
*****************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"Graph.h"
#include"List.h"
#define MAX_LEN 3

int main(int argc, char * argv[])
{
	
	if( argc != 3 ) //checking for correct number of inputs
	{
		printf("Invalid number of arguments");
		exit(1);
	}
	
	FILE *in, *out; //in and out files
	int vCount = 0; //size of graph
	int n1; //used for value storage
	int n2; //used for value storage
	int scc = 0; //number of strongly connected components
	char line[MAX_LEN];//string to hold one line from in file
	Graph G;//adj list representation of values from in file
	Graph T;//to hold transpose of G
	List S = newList();//used for DFS and finding strongly connected components
	
	
	in = fopen(argv[1], "r");
	out = fopen(argv[2], "w");
	if(in==NULL)//checking for valid in file
	{
		printf("Cannot open read file: %s\n", argv[1]);
		exit(1);
    }
	if(out==NULL) //checking for valid out file
	{
		printf("Cannot open write file: %s\n", argv[2]);
		exit(1);
    }
	
	if(fgets(line, MAX_LEN , in) != NULL)//reading num vertices from file
	{
		vCount = atoi(line);
		G = newGraph(vCount);
	}
	while(!feof(in))//reading all lines of file and building G while checking for dummy line
	{
		fscanf(in , "%d %d" , &n1 , &n2);
		if((n1 == 0) && (n2 == 0))
		{
			break;
		}
		else
		{
			addArc(G , n1 , n2);
		}
	}
	T = transpose(G);
	fprintf(out,"Adjacency list representation of G:\n");//Output of adj list rep of G
	printGraph(out, G);

	int i;
	for(i = 1; i < getOrder(G) + 1; i++)//Building S for DFS
	{
		append(S , i);
	}
	DFS(G, S);
	DFS(T , S);
	
	moveFront(S);
	for(i = 0; i < length(S); i++)//Finding num of strongly connected components
	{
		if(getParent(T , get(S)) == NIL)
		{
			++scc;
		}		
		moveNext(S);
	}
	moveBack(S);
	fprintf(out , "\n");	
	fprintf(out, "\nG contains %d strongly connected components:\n" , scc);
	
	scc = 1;
	List comps = newList();//to hold vertices of strongly connected components of G
	for(i = 0; i < length(S); i++)
	{
		prepend(comps , get(S));		
		if(getParent(T , get(S)) == 0)
		{
			fprintf(out, "Component %d: " , scc);
			++scc;
			
			printList(out , comps);
			clear(comps);		
		}
		movePrev(S);		
	}
	
	freeList(&S);
	freeList(&comps);
	freeGraph(&G);
	freeGraph(&T);
	fclose(in);
	fclose(out);
	return 0;
}
//gcc -std=c99 List.h -std=c99 List.c Graph.c Graph.h FindComponents.c