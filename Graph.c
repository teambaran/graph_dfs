/****************************************************************************************
/**
*	Graph.c : Implementation of the Graph module for PA5	
*	Name: Kaile Baran
*	Student ID: kbaran
*	Assignment: Programming Assignment 5
*	Course: CMPS 101
*****************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "Graph.h"
#define WHITE 1
#define GRAY 2
#define BLACK 3

// Exported type --------------------------------------------------------------
typedef struct GraphObj
{
	List* adj;
	int* colors;
	int* parent;
	int* discovery;
	int* finish;
	
	int time;
	int vCount;
	int eCount;
} GraphObj;

// Constructors-Destructors ---------------------------------------------------
Graph newGraph(int n)//returns reference to new graph object with n vertices and no edges
{
	Graph G = malloc(sizeof(struct GraphObj));
	
	G -> adj = calloc(n+1, sizeof(List));
	G -> colors = calloc(n+1, sizeof(int));
	G -> parent = calloc(n+1, sizeof(int));	
	G -> discovery = calloc(n+1, sizeof(int));
	G -> finish = calloc(n+1, sizeof(int));
	
	G -> time = 0;
	G -> vCount = n;
	G -> eCount = 0;

	int i;
	for(i = 1; i < n+1; i++)
	{
		G -> discovery[i] = UNDEF;
		G -> finish[i] = UNDEF;
		G -> adj[i] = newList();
		G -> colors[i] = WHITE;
		G -> parent[i] = NIL;
	}
	return G;
}

void freeGraph(Graph* pG)//frees all heap memory associated with a graph, sets
						//graph argument to NULL
{
	if(pG != NULL && *pG != NULL)
	{
		free((*pG) -> colors);
		free((*pG) -> parent);
		free((*pG) -> discovery);
		free((*pG) -> finish);
		
		int i;
		for(i = 1; i < getOrder(*pG) + 1; i++)
		{
			freeList(&((*pG) -> adj[i]));		
		}
		free((*pG) -> adj);
		free(*pG);
		*pG = NULL;
	}	
}

// Access functions -----------------------------------------------------------

int getOrder(Graph G)//number of vertices in G
{
	if(G == NULL)
	{
		printf("Graph Error: calling getOrder() on NULL Graph reference\n");
		exit(1);
	}
	return G -> vCount;
}

int getSize(Graph G)//number of edges in G
{
	if(G == NULL)
	{
		printf("Graph Error: calling getSize() on NULL Graph reference\n");
		exit(1);
	}
	return (G -> eCount);
}

int getParent(Graph G, int u)//parent of specified vertex can be NIL
{
	if(G == NULL)
	{
		printf("Graph Error: calling getParent() on NULL Graph reference\n");
		exit(1);
	}
	if((u < 1) || (u > getOrder(G)))//pre: 1<=U<=getOrder(G)
	{
		printf("Graph Error: calling getParent() on invalid vertex\n");
		exit(1);
	}
	return (G -> parent[u]);	
}

int getDiscover(Graph G , int u)//discovery time of specified vertex
								//pre: 0<u<=getOrder(G)
{
	if(G == NULL)
	{
		printf("Graph Error: calling getDiscover() on NULL Graph reference\n");
		exit(1);
	}
	if((u < 1) || (u > getOrder(G)))
	{
		printf("Graph Error: calling getDiscover() on invalid vertex\n");
		exit(1);
	}
	return (G -> discovery[u]);
}

int getFinish(Graph G , int u)//finish time of specified vertex
							 //pre: 0<u<=getOrder(G)
{
	if(G == NULL)
	{
		printf("Graph Error: calling getFinish() on NULL Graph reference\n");
		exit(1);
	}
	if((u < 1) || (u > getOrder(G)))
	{
		printf("Graph Error: calling getFinish() on invalid vertex\n");
		exit(1);
	}
	return (G -> finish[u]);
}


// Manipulation procedures ----------------------------------------------------

void orderedInsert(List L, int v)//helper method for adding edges in ascending order
{
	if(length(L) == 0)
	{
		append(L , v);
	}
	else
	{
		moveFront(L);
		int inserted = 0;
		while((index(L) != -1) && (inserted == 0))
		{
			if(get(L) > v)
			{
				insertBefore(L , v);
				inserted = 1;
			}
			moveNext(L);
		}
		if(inserted == 0)
		{
			append(L , v);
		}
	}		
}

void addEdge(Graph G, int u, int v)//adds an edge between the specified vertices u and v (undirected)
								  //pre: 1<=u<=getOrder() && 1<=v<=getOrder()
{
	if(G == NULL)
	{
		printf("Graph Error: calling addEdge() on NULL Graph reference\n");
		exit(1);
	}
	if((u < 1) || u > (getOrder(G)))
	{
		printf("Graph Error: calling addEdge() on invalid vertex u\n");
		exit(1);
	}
	if((v < 1) || (v > getOrder(G)))
	{
		printf("Graph Error: calling addEdge() on invalid vertex v\n");
		exit(1);
	}
	orderedInsert((G -> adj[u]) , v);
	orderedInsert((G -> adj[v]) , u);
	(G -> eCount)++;
}

void addArc(Graph G, int u, int v)//adds an edge between the specified vertices u and v (directed from u to v)
								 //pre: 1<=u<=getOrder() && 1<=v<=getOrder()
{
	if(G == NULL)
	{
		printf("Graph Error: calling addArc() on NULL Graph reference\n");
		exit(1);
	}
	if((u < 1) || u > (getOrder(G)))
	{
		printf("Graph Error: calling addArc() on invalid vertex u\n");
		exit(1);
	}
	if((v < 1) || (v > getOrder(G)))
	{
		printf("Graph Error: calling addArc() on invalid vertex v\n");
		exit(1);
	}
	orderedInsert((G -> adj[u]) , v);
	(G -> eCount)++;
}

void visit(Graph G, List S, int v, int time)//recursive method used by DFS
{
	int u;
	G -> colors[v] = GRAY;
	++(G -> time);
	G -> discovery[v] = G -> time;
	
	moveFront(G->adj[v]);	
	while(index(G -> adj[v]) != -1)
	{
		u = get(G -> adj[v]);
		if((G -> colors[u]) == WHITE)
		{			
			G -> parent[u] = v;
			visit(G , S , u , G -> time);
		}
		moveNext(G -> adj[v]);
	}
	G -> colors[v] = BLACK;
	++(G -> time);
	G -> finish[v] = G -> time;
	prepend(S , v);
}

void DFS(Graph G, List S)//Performs DFS on G
						//pre: length(S) == getOrder(G)
{
	if(G == NULL)
	{
		printf("Graph Error: calling DFS() on NULL Graph reference\n");
		exit(1);
	}
	if(S == NULL)
	{
		printf("Graph Error: calling DFS() on NULL List reference\n");
		exit(1);
	}
	if(length(S) != getOrder(G))
	{
		printf("Graph Error: calling DFS() with invalid length of S\n");
		exit(1);
	}
	
	int i;
	G -> time = 0;
	for(i = 1; i < getOrder(G) + 1; i++)
	{
		G -> colors[i] = WHITE;
		G -> parent[i] = NIL;
	}
	List temp = copyList(S);
	clear(S);
	moveFront(temp);
	int v;
	while(index(temp) != -1)
	{
		v = get(temp);
		if( G -> colors[v] == WHITE)
		{
			visit(G , S , v , G -> time);
		}
		moveNext(temp);
	}
	freeList(&temp);
}
// Other Functions ----------------------------------------------------

Graph transpose(Graph G)//returns the transpose of G
{
	if(G == NULL)
	{
		printf("Graph Error: calling transpose() on NULL Graph reference\n");
		exit(1);
	}
	Graph tranG = newGraph(getOrder(G));
	int i;
	for(i = 1; i < getOrder(tranG) + 1; i++)
	{
		moveFront(G -> adj[i]);
		while(index(G -> adj[i]) != -1)
		{
			addArc(tranG , get(G -> adj[i]) , i);
			moveNext(G -> adj[i]);			
		}
	}
	return tranG;
}

Graph copyGraph(Graph G)//returns a copy of G
{
	if(G == NULL)
	{
		printf("Graph Error: calling copyGraph() on NULL Graph reference\n");
		exit(1);
	}
	Graph copyG = newGraph(getOrder(G));

	int j;
	for(j = 1; j < getOrder(copyG) + 1; j++)
	{
		copyG -> adj[j] = copyList(G -> adj[j]);
	}
	
	return copyG;
}

void printGraph(FILE* out, Graph G)//prints G to the specified output file
{
	if(G == NULL)
	{
		printf("Graph Error: calling printGraph() on NULL Graph reference\n");
		exit(1);
	}
	if(out==NULL)
	{
		printf("Graph Error: calling printGraph() on NULL FILE* reference\n");
		exit(1);
	}
	int i;
	for(i = 1; i < getOrder(G) + 1; i++)
	{
		fprintf(out , "%d: " , i);
		printList(out , G -> adj[i]);
	}
}













	