/****************************************************************************************
/**
*	GraphTest.c : Contains main for pa5. Performs tests on Graph ADT in isolation using a
				  user specified output file to verify proper output format.
*	Name: Kaile Baran
*	Student ID: kbaran
*	Assignment: Programming Assignment 5
*	Course: CMPS 101
*****************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"Graph.h"
#include"List.h"

int main(int argc, char * argv[])
{
	if( argc != 2)
	{
		printf("Invalid number of arguments");
		exit(1);
	}
	FILE *out;
	out = fopen(argv[1], "w");
	if(out==NULL)
	{
		printf("Cannot open write file: %s\n", argv[1]);
		exit(1);
    }
	
	Graph G = newGraph(8);
	Graph T;
	List S = newList();
	addArc(G , 1 , 2);
	addArc(G , 2 , 3);
	addArc(G , 2 , 5);
	addArc(G , 2 , 6);
	addArc(G , 3 , 4);
	addArc(G , 3 , 7);
	addArc(G , 4 , 3);
	addArc(G , 4 , 8);
	addArc(G , 5 , 1);
	addArc(G , 5 , 6);
	addArc(G , 6 , 7);
	addArc(G , 7 , 6);
	addArc(G , 7 , 8);
	addArc(G , 8 , 8);
	printGraph(out, G);
	
	T = transpose(G);
	fprintf(out, "\n");
	printGraph(out, T);
	
	int i;
	for(i = 1; i < getOrder(G) + 1; i++)
	{
		append(S , i);
	}
	DFS(G , S);
	fprintf(out, "\nG:\n");
	printList(out, S);
	
	DFS(T , S);
	fprintf(out, "\nT:\n");
	printList(out, S);	
	
	freeList(&S);
	freeGraph(&G);
	freeGraph(&T);
	fclose(out);
	return 0;
}
//gcc -std=c99 List.h -std=c99 List.c Graph.c Graph.h GraphTest.c